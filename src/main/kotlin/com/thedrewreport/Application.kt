package com.thedrewreport


import com.apurebase.kgraphql.GraphQL
import com.rometools.rome.feed.synd.SyndFeed
import com.rometools.rome.io.SyndFeedInput
import com.rometools.rome.io.XmlReader
import com.thedrewreport.plugins.*
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.transaction
import java.net.URL


fun main() {
    val db = SqliteDb()
    createSchema()
    val url = "https://bitcoinmagazine.com/feed"
    val feed: SyndFeed = SyndFeedInput().build(XmlReader(URL(url)))
    ExposedFeed.create(feed)
    embeddedServer(Netty, port = 8080, host = "0.0.0.0") {
        configureRouting()
        configureSecurity()
        configureMonitoring()
        configureTemplating()
        configureSerialization()
        configureSockets()
        configureKGraphQL()
    }.start(wait = true)

}

fun createSchema() {
    transaction {
        addLogger(StdOutSqlLogger)
        SchemaUtils.create(ExposedFeeds)
        SchemaUtils.create(ExposedEntries)
    }
}

fun Application.configureKGraphQL() {
    install(GraphQL) {
        useDefaultPrettyPrinter = true
        playground = true
        endpoint = "/graphql"
        schema { RSSFeedschema() }
    }
}
