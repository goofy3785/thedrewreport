package com.thedrewreport

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

object ExSyndFeed : IntIdTable() {

}

object RSSFeeds :IntIdTable() {
    val name = varchar("name", length = 50) // Column<String>
}
class RSSFeed(id: EntityID<Int>): IntEntity(id){
    companion object : IntEntityClass<RSSFeed>(RSSFeeds)
    var name by RSSFeeds.name
}
object RSSs : IntIdTable() {
    val version = varchar("version", length = 50) // Column<String>
}
class RSS(id: EntityID<Int>): IntEntity(id) {
    companion object : IntEntityClass<Channel>(Channels)
    val version by RSSs.version
}

//<title>Liftoff News</title>
//<link>http://liftoff.msfc.nasa.gov/</link>
//<description>Liftoff to Space Exploration.</description>
//<language>en-us</language>
//<pubDate>Tue, 10 Jun 2003 04:00:00 GMT</pubDate>
//<lastBuildDate>Tue, 10 Jun 2003 09:41:01 GMT</lastBuildDate>
//<docs>http://blogs.law.harvard.edu/tech/rss</docs>
//<generator>Weblog Editor 2.0</generator>
//<managingEditor>editor@example.com</managingEditor>
//<webMaster>webmaster@example.com</webMaster>
//<item>
object Channels : IntIdTable() {
    val title = varchar("title", 50) // Column<String>
    val link = varchar("link", 50) // Column<String>
    val description = text("description", null, true) // Column<String>
    val pubDate = varchar("pubDate", 50) // Column<String>
    val lastBuildDate = varchar("lastBuildDate", 50) // Column<String>
    val docs = varchar("docs", 50) // Column<String>
    val generator = varchar("generator", 50) // Column<String>
    val managingEditor = varchar("managingEditor", 50) // Column<String>
    val webMaster = varchar("webMaster", 50) // Column<String>
    val item = varchar("item", 50) // Column<String>

    val rss = reference("RSSs", RSSs)
}
class Channel(id: EntityID<Int>): IntEntity(id) {
    companion object : IntEntityClass<Channel>(Channels)
    var title by Channels.title
    var link by Channels.link
    var description by Channels.description
    var pubDate by Channels.pubDate
    var lastBuildDate by Channels.lastBuildDate
    var docs by Channels.docs
    var generator by Channels.generator
    var managingEditor by Channels.managingEditor
    var webMaster by Channels.webMaster
    var item by Channels.item
    var rss by Channel referencedOn Channels.rss

}

//<title>Star City</title>
//<link>
//http://liftoff.msfc.nasa.gov/news/2003/news-starcity.asp
//</link>
//<description>
//How do Americans get ready to work with Russians aboard the International Space Station? They take a crash course in culture, language and protocol at Russia's <a href="http://howe.iki.rssi.ru/GCTC/gctc_e.htm">Star City</a>.
//</description>
//<pubDate>Tue, 03 Jun 2003 09:39:21 GMT</pubDate>
//<guid>
//http://liftoff.msfc.nasa.gov/2003/06/03.html#item573
//</guid>
object Items : IntIdTable() {
    val title = varchar("title", 50) // Column<String>
    val link = varchar("link", 50) // Column<String>
    val description = text("description", null, true) // Column<String>
    val pubDate = varchar("pubDate", 50) // Column<String>
    val guid = varchar("guid", 50) // Column<String>

    val channel = reference("Channels", Channels)
}
class Item(id: EntityID<Int>): IntEntity(id) {
    companion object : IntEntityClass<Item>(Items)
    var title by Items.title
    var link by Items.link
    var description by Items.description
    var pubDate by Items.description
    var guid by Items.description

    var channel by Channel referencedOn Items.channel
}

