package com.thedrewreport

import com.rometools.rome.feed.synd.SyndEntry
import com.rometools.rome.feed.synd.SyndFeed
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.transactions.transaction
import java.text.SimpleDateFormat
import java.util.*

//@Serializable
//data class JSONFeed(
//    val description: String?,
//    val docs: String?,
//    val feedType: String?,
//    val link: String?,
//    val author: String?,
//    val authors: String?,
//    val categories: String?,
//    val language: String?,
//    val title: String?,
//    val uri: String?,
//    val webMaster: String?,
//    val timestamp: String?
//) {
//    constructor(exposedFeed: ExposedFeed) : this(
//        exposedFeed.description,
//        exposedFeed.docs,
//        exposedFeed.feedType,
//        exposedFeed.link,
//        exposedFeed.author,
//        exposedFeed.authors,
//        exposedFeed.categories,
//        exposedFeed.language,
//        exposedFeed.title,
//        exposedFeed.uri,
//        exposedFeed.webMaster,
//        exposedFeed.timestamp
//    )
//    constructor() : this("", "", "", "", "", "", "", "", "", "", "", "")
//}

object ExposedFeeds : IntIdTable() {
    val description = text("description")
    val docs = varchar("docs", 200).nullable()
    val feedType = varchar("feedType", 200).nullable()
    val link = varchar("link", 200).nullable()
    val author = varchar("author", 200).nullable()
    val authors = varchar("authors", 200).nullable()
    val categories = varchar("categories", 200).nullable()
    val language = varchar("language", 200).nullable()
    val title = varchar("title", 200).nullable()
    val uri = varchar("uri", 200).nullable()
    val webMaster = varchar("webMaster", 200).nullable()
    val timestamp = varchar("timestamp", 200).nullable()
}

class ExposedFeed(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<ExposedFeed>(ExposedFeeds) {
        val sdf = SimpleDateFormat("YYYY-MM-DD HH:mm:ss")
        fun create(syndFeed: SyndFeed): ExposedFeed {
            val feed = transaction {
                ExposedFeed.new {
                    description = syndFeed.description
                    docs = syndFeed.docs
                    feedType = syndFeed.feedType
                    link = syndFeed.link
                    author = syndFeed.author
                    authors = syndFeed.authors.map { it.name }.joinToString(", ")
                    categories = syndFeed.categories.map { it.name }.joinToString(", ")
                    language = syndFeed.language
                    title = syndFeed.title
                    uri = syndFeed.uri
                    webMaster = syndFeed.webMaster
                    timestamp = sdf.format(Date().time)//TODO fix the funny date format
                }
            }
            syndFeed.entries.forEach { ExposedEntry.create(it, feed) }
            return feed
        }
    }

    var description by ExposedFeeds.description
    var docs by ExposedFeeds.docs
    var feedType by ExposedFeeds.feedType
    var link by ExposedFeeds.link
    var author by ExposedFeeds.author
    var authors by ExposedFeeds.authors
    var categories by ExposedFeeds.categories
    var language by ExposedFeeds.language
    var title by ExposedFeeds.title
    var uri by ExposedFeeds.uri
    var webMaster by ExposedFeeds.webMaster
    var timestamp by ExposedFeeds.timestamp

}

object ExposedEntries : IntIdTable() {
    val title = varchar("title", 10000)
    val author = varchar("author", 10000).nullable()
    val description = text("description").nullable()
    val categories = text("categories").nullable()
    val contents = text("contents").nullable()
    val comments = text("comments").nullable()
    val uri = varchar("uri", 1000).nullable()
    val publishedDate = varchar("publishedDate", 10000).nullable()
    val updatedDate = varchar("updatedDate", 10000).nullable()

    val exposedFeed = reference("Feed", ExposedFeeds)
}

class ExposedEntry(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<ExposedEntry>(ExposedEntries) {
        fun create(syndEntry: SyndEntry, feed: ExposedFeed) {

            return transaction {
                ExposedEntry.new {
                    title = syndEntry.title
                    author = syndEntry.author
                    description = syndEntry.description.value
                    categories = syndEntry.categories.map { it.name }.joinToString(",")
                    contents = syndEntry.contents.map { it.value }.joinToString(",")
                    comments = syndEntry.comments
                    uri = syndEntry.uri
                    publishedDate = syndEntry.publishedDate?.toString()
                    updatedDate = syndEntry.updatedDate?.toString()
                    exposedFeed = feed
                }
            }
        }
    }

    var title by ExposedEntries.title
    var author by ExposedEntries.author
    var description by ExposedEntries.description
    var categories by ExposedEntries.categories
    var contents by ExposedEntries.contents
    var comments by ExposedEntries.comments
    var uri by ExposedEntries.uri
    var publishedDate by ExposedEntries.publishedDate
    var updatedDate by ExposedEntries.updatedDate

    var exposedFeed by ExposedFeed referencedOn ExposedEntries.exposedFeed
}

