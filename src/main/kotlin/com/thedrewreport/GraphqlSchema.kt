package com.thedrewreport

import com.apurebase.kgraphql.KGraphQL
import com.apurebase.kgraphql.schema.dsl.SchemaBuilder
import org.jetbrains.exposed.sql.transactions.transaction

fun SchemaBuilder.RSSFeedschema() {
    // create query returns a list of all RSS feeds
    query("RSSFeeds") {
        resolver {
                -> transactionEnvironment {
                ExposedFeed.all()
            }
        }
    }

    // kotlin classes need to be registered with "type" method
    // to be included in created schema type system
    // class Character is automatically included,
    // as it is return type of both created queries
    type<ExposedFeed> {
        ExposedFeed::id.ignore()
        ExposedFeed::klass.ignore()
        ExposedFeed::db.ignore()
        ExposedFeed::writeValues.ignore()
        ExposedFeed::_readValues.ignore()
        ExposedFeed::readValues.ignore()
    }
    type<ExposedEntry> {
        ExposedEntry::id.ignore()
        ExposedEntry::klass.ignore()
        ExposedEntry::db.ignore()
        ExposedEntry::writeValues.ignore()
        ExposedEntry::_readValues.ignore()
        ExposedEntry::readValues.ignore()
    }
}