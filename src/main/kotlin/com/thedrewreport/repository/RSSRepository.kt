//package com.thedrewreport.repository
//
//import Repository
//import com.thedrewreport.ExposedFeed
//import com.thedrewreport.ExposedFeeds
//import com.thedrewreport.transactionEnvironment
//import org.jetbrains.exposed.sql.ResultRow
//import org.jetbrains.exposed.sql.SizedIterable
//import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
//import org.jetbrains.exposed.sql.select
//
//fun ExposedFeeds.toDataObj(row: ResultRow) = ExposedFeed(row[id])
//
//object FirmRepository : Repository<ExposedFeed> {
//    override fun add(element: ExposedFeed): Int = 0
//
//    override fun getElement(indexer: Int): ExposedFeed =
//        transactionEnvironment {
//            ExposedFeeds
//                .select { ExposedFeeds.id eq indexer }
//                .map { x -> ExposedFeeds.toDataObj(x) }
//                .first()
//        }
////    fun getElement(title: String): ExposedFeed =
////        transactionEnvironment {
////            ExposedFeeds
////                .select { ExposedFeeds.title eq title }
////                .map { x -> ExposedFeeds.toDataObj(x) }
////                .first()
////        }
////
////    override fun getAll(): SizedIterable<ExposedFeed> {
////        return transactionEnvironment {
////            ExposedFeed.all()
////        }
////    }
//}