import React from 'react';
import logo from './logo.svg';
import './App.css';
import MainNewsFeed from "./MainNewsFeed";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <MainNewsFeed/>
      </header>
    </div>
  );
}

export default App;
