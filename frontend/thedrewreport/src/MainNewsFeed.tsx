import React from 'react';


import {
    Col,
    Container,
    Row
} from "react-bootstrap";

class MainNewsFeed extends React.Component {
    constructor(props: any) {
        super(props);
    }

    render() {
        return (
            <NewsFeedCol/>
        );
    }
}

class NewsFeedCol extends React.Component {
    constructor(props: any) {
        super(props);
    }
    render() {
        return (
            <Row>
                <Col sm={12} md={6} lg={4}>
                    <NewsFeedItem index={1}/>
                </Col>
            </Row>
        );
    }
}

interface NewsFeedItemProps {
    index: number
}

class NewsFeedItem extends React.Component<NewsFeedItemProps> {

    constructor(props: NewsFeedItemProps) {
        super(props);
    }

    render() {
        const theIndex = this.props.index
        return (
            <span className="newsFeedItem">
            </span>
        );
    }
}

export default MainNewsFeed;
